import './App.css';
import { Route, Routes, BrowserRouter  } from 'react-router-dom';
import { lazy, useState, createContext } from 'react';
import Navbar from './component/Navbar';
import Footer from './component/Footer';

import HomePage from'./pages/Home/HomePage';
import ListPage from './pages/List/ListPage';
import NewsPage from './pages/News/NewsPage';
import EditorPage from './pages/Editor/EditorPage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';

export const LangaugeContext = createContext(null);

const App = () => {
  const [langauge, setLangauge] = useState('TH');
  
  return (
    <div className='Page'>
      <LangaugeContext.Provider value={{langauge, setLangauge}}>
        <Navbar/>
        <div className='Container'>
          <BrowserRouter>
              <Routes>
                <Route path="/" element={<HomePage/>} />
                <Route path="/news" element={<ListPage/>} />
                <Route path="/news/read/:id" element={<NewsPage/>} />
                <Route path="/news/create" element={<EditorPage/>} />
                <Route path="/news/edit/:id" element={<EditorPage/>} />
                <Route path="*" element={<NotFoundPage />} />
              </Routes>
          </BrowserRouter>
        </div> 
        <Footer />
      </LangaugeContext.Provider>
    </div>
    
  );
}

export default App;
