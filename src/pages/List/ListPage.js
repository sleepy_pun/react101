import WebMap from "../../component/WebMap"
import useData from "../../hook/useData"
import { LangaugeContext } from "../../App"
import { useContext } from "react"
import { useNavigate } from "react-router-dom"

import '../../asset/ListPage.css';

const ListTable = (props) => {
    const data = props.data ?? []
    const navigate = useNavigate()
    if (data.length === 0){
        return (
            <div className="EmptyDataTable">{props.lang === 'TH'? 'เงียบเหงาจัง ไม่มีข่าวสารอัพเดทเลย': 'Very quiet, no updates at all.'}</div>
        )
    }
    const newsList = []
    for(const news of data){
        const date = new Date(news.updated_at);
        newsList.push(<tr>
            <td className="ID"><div>{news.id}</div></td>
            <td className="Title">{props.lang === 'TH'? news.title.th : news.title.en}</td>
            <td className="Category">{props.categories[news.category_id]}</td>
            <td className="UpdateTime">{`${date.getDate()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes() > 10? date.getMinutes(): `0${date.getMinutes()}`}${props.lang === 'TH'? 'น.' : ''}`}</td>
            <td className="ToDo">
                <button className="ToDoButton" onClick={() => navigate(`/news/edit/${news.id}`)}>{props.lang === 'TH'? "แก้ไข" : "Edit"}</button>
                <button className="ToDoButton" onClick={() => navigate(`/news/read/${news.id}`)}>{props.lang === 'TH'? "รายละเอียด" : "Detail"}</button>
            </td>
        </tr>)
    }
    return (<table className="ListTable">
        <tr>
            <th className="ID"><div></div></th>
            <th className="Title">{props.lang === 'TH'? 'ชื่อเรื่อง' : 'Title'}</th>
            <th className="Category">{props.lang === 'TH'? 'หมวดหมู่' : 'Category'}</th>
            <th className="UpdateTime">{props.lang === 'TH'? 'วันที่แก้ไข' : 'Updated time'}</th>
            <th className="ToDo"></th>
        </tr>
        {newsList}
    </table>)
}

const ListPage = () => {
    const {langauge} = useContext(LangaugeContext)
    const {data, category} = useData()
    const navigate = useNavigate()
    return (<>
        <WebMap />
        <form className="ListSearchForm">
            <div className="ListSearchInput">
                <label>
                    {langauge === 'TH'? 'ชื่อเรื่อง' : 'Title'}
                    <input type="text" />
                </label>
            </div>
            <div className="ListSearchInput">
                <label>
                    {langauge === 'TH'? 'หมวดหมู่' : 'Category'}
                    <select>
                        <option value={"all"} selected>{langauge === 'TH'? 'ทั้งหมด' : 'All Category'}</option>
                        <option value={1} >{langauge === 'TH'? 'ข่าวสาร' : 'News'}</option>
                        <option value={2} >{langauge === 'TH'? 'ข่าวสาร' : 'News'}</option>
                        <option value={3} >{langauge === 'TH'? 'แจ้งเตือน' : 'Notification'}</option>
                    </select>
                </label>
            </div>
            <div className="ListSearchInput">
                <button type="submit" value="Submit" className="SearchButton">{langauge === 'TH'? 'ค้นหา' : 'Search'}</button>
            </div>
            
        </form>
        <button className="ToDoButton" onClick={() => navigate('/news/create', {replace: true})}>{langauge === 'TH'? '+ สร้างข่าวสาร' : '+ Create News'}</button>
        <ListTable lang={langauge} data={data} categories={category}/>
    </>  
    )
}
export default ListPage