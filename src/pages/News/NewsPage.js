import WebMap from "../../component/WebMap"
import useData from "../../hook/useData"
import { LangaugeContext } from "../../App"
import { useContext } from "react"
import { useParams, useNavigate } from 'react-router-dom';
import '../../asset/NewsPage.css'

const NewsPage = () => {
    const {id} = useParams()
    const {data, category} = useData()
    const {langauge} = useContext(LangaugeContext)
    const newsData = data.find((n) => n.id === Number(id))
    const publishDate = new Date(newsData.publish_at);
    const updateDate = new Date(newsData.updated_at);
    const navigate = useNavigate()
    return (<div>
        <WebMap page="read" title={newsData.title} id={id}/>
            <div className="TemplateSideBar">
                <div className="MainContent">
                    <div className="ContentBlock-L">
                        <div className="Label">{langauge === 'TH'? 'ชื่อเรื่อง (ภาษาไทย)': 'Title (Thai)'}</div>
                        <h2>{newsData.title.th}</h2>
                    </div>
                    <div className="ContentBlock-L">
                        <div className="Label">{langauge === 'TH'? 'ชื่อเรื่อง (ภาษาอังกฤษ)': 'Title (English)'}</div>
                        <h2>{newsData.title.en}</h2>
                    </div>
                    <div className="ContentBlock-L">
                        <div className="Label">{langauge === 'TH'? 'หมวดหมู่': 'Category'}</div>
                        <h3>{category[newsData.category_id]}</h3>
                    </div>
                    <div className="ContentBlock-L">
                        <div className="Label">{langauge === 'TH'? 'วันที่เปิดให้ใช้งาน': 'Publish at'}</div>
                        <h3>{`${publishDate.getDate()}/${publishDate.getMonth()}/${publishDate.getFullYear()} ${publishDate.getHours()}:${publishDate.getMinutes()}${langauge === 'TH'? 'น.' : ''}`}</h3>
                    </div>
                    <div className="ContentBlock-L">
                        <div className="Label">{langauge === 'TH'? 'แก้ไขเมื่อ': 'Update at'}</div>
                        <h3>{`${updateDate.getDate()}/${updateDate.getMonth()}/${updateDate.getFullYear()} ${updateDate.getHours()}:${updateDate.getMinutes()}${langauge === 'TH'? 'น.' : ''}`}</h3>
                    </div>
                    <div className="ContentBlock-L">
                        <button className="ContentButton" onClick={() => navigate(`/news/edit/${id}`)}>{langauge === 'TH'? 'แก้ไข': 'Edit'}</button>
                    </div>
                </div>
                <div className="SideBarContent">
                    <div className="ContentBlock-R">
                        <img className="ContentImage" src={newsData.image} />
                    </div>
                </div>
            </div>
    </div>
            
    )    
}
export default NewsPage
