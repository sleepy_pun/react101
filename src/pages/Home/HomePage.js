import useData from "../../hook/useData"
import { LangaugeContext } from "../../App"
import { useContext } from "react"
import { useNavigate } from "react-router-dom";



const HomePageNews = (props) => {
    const navigate = useNavigate();
    const newsData = props.data? props.data : []
    const newsList = []
    for( const i in newsData){
        if(i >= 4){
            break
        }
        else{
            newsList.push(
                <div className="NewsChild" onClick={() => navigate(`/news/read/${newsData[i].id}`)}>
                    <img src={newsData[i].image} />
                    <div className="Overlay">
                        <p>{props.lang === 'TH' ? newsData[i].title.th : newsData[i].title.en}</p>
                    </div>
                </div>
            )
        }
    }
    return (
        <div className="HomeListContainer">
            {newsList}
        </div>
    )
}

const HomePage = () => {
    const {data} = useData()
    const {langauge} = useContext(LangaugeContext)
    const navigate = useNavigate();
    console.log(data)
    return (<>
        <div className="HeadImage">
            <img src="https://source.unsplash.com/random/?news" />
        </div>
        <div className="HomeList">
            <div className="HomeListTitle">
                <div><h2>{langauge === 'TH' ? "ข่าวสาร" : "News"}</h2></div>
                <div className="HomeListTitleButton"><button onClick={() => navigate('/news')} className="SeeAllButton">{langauge === 'TH' ? "ดูทั้งหมด" : "See All"}</button></div>
            </div>
            <HomePageNews data={data} lang={langauge} />
        </div>
    </>)
}

export default HomePage