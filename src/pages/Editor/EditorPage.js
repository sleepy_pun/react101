import WebMap from "../../component/WebMap";
import useData from "../../hook/useData";
import { LangaugeContext } from "../../App";
import { useContext } from "react";
import { useParams, useNavigate } from 'react-router-dom';
import '../../asset/NewsPage.css';
import '../../asset/EditorPage.css';
import PlusIcon from '../../asset/PlusIcon.svg';

const EditorPage = () => {
    const navigate = useNavigate()
    const {data} = useData()
    const {langauge} = useContext(LangaugeContext)
    const path = window.location.pathname
    const submit = (value) => {
        console.log(value)
    }
    const isEdit = path !== "/news/create"
    const {id} = useParams()
    let editData = null
    let uPublishDate = null
    if (isEdit) {
        
        editData = data.find((n) => n.id === Number(id))
        const publishDate = new Date(editData.publish_at)
        uPublishDate = `${publishDate.getFullYear()}-${publishDate.getMonth() < 10? `0${publishDate.getMonth()}`: publishDate.getMonth()}-${publishDate.getDate() < 10? `0${publishDate.getDate()}`: publishDate.getDate()}T${publishDate.getHours() < 10? `0${publishDate.getHours()}`: publishDate.getHours()}:${publishDate.getMinutes() < 10? `0${publishDate.getMinutes()}`: publishDate.getMinutes()}:${publishDate.getSeconds() < 10? `0${publishDate.getSeconds()}`: publishDate.getSeconds()}`
    }
    return(
        <>
            {/*<WebMap id={id} page={isEdit? 'edit': 'create'} />*/}
            {isEdit ? <WebMap page="edit" title={editData.title} id={id}/>: <WebMap page="create" />}
            <form onSubmit={submit}>
                <div className="TemplateSideBar">
                    <div className="MainContent">
                        <div className="ContentBlock-L">
                            <div className="Label">{langauge === 'TH'? 'ชื่อเรื่อง (ภาษาไทย)': 'Title (Thai)'}</div>
                            <input name="thaiTitle" className="InputTitle" type='text' value={isEdit? editData.title.th: ''}/>
                        </div>
                        <div className="ContentBlock-L">
                            <div className="Label">{langauge === 'TH'? 'ชื่อเรื่อง (ภาษาอังกฤษ)': 'Title (English)'}</div>
                            <input name="englishTitle" className="InputTitle" type='text' value={isEdit? editData.title.en: ''}/>
                        </div>
                        <div className="ContentBlock-L">
                            <div className="Label">{langauge === 'TH'? 'หมวดหมู่': 'Category'}</div>
                            <select name="category" className="SelectInput" value={isEdit? editData.category_id: ''}>
                                <option value={1} >{langauge === 'TH'? 'ข่าวสาร' : 'News'}</option>
                                <option value={2} >{langauge === 'TH'? 'ข่าวสาร' : 'News'}</option>
                                <option value={3} >{langauge === 'TH'? 'แจ้งเตือน' : 'Notification'}</option>
                            </select>
                        </div>
                        <div className="ContentBlock-L">
                            <div className="Label">{langauge === 'TH'? 'วันที่เปิดให้ใช้งาน': 'Publish at'}</div>
                            <input name="createTime" className="SelectInput" type="datetime-local" value={isEdit? uPublishDate: ''}/>
                        </div>
                        <div className="ContentBlock-L">
                            <button className="EndButton" onClick={() => navigate('/news')}>{langauge === 'TH'? 'ยกเลิก': 'Cancel'}</button>
                            <button className="EndButton PrimaryButton" type="submit">{langauge === 'TH'? 'บันทึก': 'Save'}</button>
                        </div>
                    </div>
                    <div className="SideBarContent">
                        <div className="ContentBlock-R">
                            {isEdit && <button className="ContentButton">Change Picture</button>}
                            {isEdit && (
                                <img className="ContentImage" src={editData.image}/>
                            )}
                            {!isEdit && (<button className="ImageUploadButton"><img src={PlusIcon}/></button>)}
                        </div>
                    </div>
                </div>
            </form>
        </>
    )
}

export default EditorPage