import React from "react";
import { useNavigate } from "react-router-dom";
import { LangaugeContext } from "../../App";
import { useContext } from "react";
import '../../asset/NotFoundPage.css';
import '../../asset/NewsPage.css';

const NotFoundPage = () => {
    const navigate = useNavigate()
    const {langauge} = useContext(LangaugeContext)
    return (
        <div className="BlockContainer">
            <h1 className="HeadLowMargin">{langauge === 'TH' ? "เกิดข้อผิดพลาด": "Error"}</h1>
            <h1 className="UltraBig">404</h1>
            <p className="Description">{langauge === 'TH' ? "โอ้ม่ายยยย นี่มันลิ้งค์อะไรกันเนี่ย เราหาไม่เจอน่ะ ยังไงก็กลับไปเริ่มที่หน้าแรกก่อนแล้วกันนะ": "Oh my, what's this link? We can't find it Anyway, let's go back to the first page first."}</p>
            <button className="ContentButton" onClick={() => navigate('/')}>{langauge === 'TH' ? "กลับไปหน้าแรก": "Return to home"}</button>
        </div>
    )
    
}
export default NotFoundPage