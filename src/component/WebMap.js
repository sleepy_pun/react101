import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { LangaugeContext } from "../App";
const WebMap = (props) => {
    const navigate = useNavigate();
    const path = window.location.pathname
    const {langauge} = useContext(LangaugeContext)
    return (
        <div className="WebMap">
            <button className="WebMapButton" onClick={() => navigate('/', {replace: true})}><h1>{langauge === 'TH'? 'หน้าแรก' : 'Homepage'}</h1></button>
            { path !== '/' && (<>
                {'>'}
                <button className={(path === "/news") ? "WebMapCurrentButton": "WebMapButton"} onClick={() => navigate('/news')}>
                    <h1>{langauge === 'TH'? 'ข่าวสาร' : 'News'}</h1>
                </button>
            </>)}
            { (props.page === 'read' || props.page === 'edit')  && (<>
                {'>'}
                <button className={(props.page === 'read') ? "WebMapCurrentButton": "WebMapButton"} onClick={() => navigate(`/news/read/${props.id}`)}>
                    <h1>{langauge === 'TH'? props.title.th : props.title.en}</h1>
                </button>
            </>)}
            { props.page === 'edit' && (<>
                {'>'}
                <button className={(props.page === 'edit') ? "WebMapCurrentButton": "WebMapButton"} onClick={() => navigate(`/news/edit/${props.id}`)}>
                    <h1>{langauge === 'TH'? 'แก้ไข' : 'Edit'}</h1>
                </button>
            </>)}
            { props.page === 'create' && (<>
                {'>'}
                <button className={(props.page === 'create') ? "WebMapCurrentButton": "WebMapButton"} onClick={() => navigate(path)}>
                    <h1>{langauge === 'TH'? 'สร้างข่าวสาร' : 'Create News'}</h1>
                </button>
            </>)}
        </div>
    )
}
export default WebMap