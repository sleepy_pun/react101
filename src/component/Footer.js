import { useContext } from 'react'
import { LangaugeContext } from '../App'

const Footer = () => {
    const { langauge } = useContext(LangaugeContext)
    return (
        <div className='Footer'>
            <p>{langauge === 'TH'? `© สงวนลิขสิทธิ์ ให้กับพี่เพชร และพี่เบลเท่านั้น เขียนโค้ดโดยปั้น`: `© All rights reserved to P'Petch and P'Belle only. Coding by Pun `}</p>
        </div>
    )
}

export default Footer