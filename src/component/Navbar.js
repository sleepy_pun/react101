import logo from '../asset/icon.png'
import { useContext } from 'react'
import { LangaugeContext } from '../App'

const LangaugeSelector = () => {
    const { langauge, setLangauge } = useContext(LangaugeContext)
    return (
        <div className='LanguageSelector'>
            <button 
                className={langauge === 'TH'? 'ActiveLangaugeButton' : 'LangaugeButton'} 
                onClick={() => {
                    setLangauge('TH')
                    console.log('to TH')
                }}
            >TH</button>
            |
            <button 
                className={langauge === 'EN'? 'ActiveLangaugeButton' : 'LangaugeButton'} 
                onClick={() => {
                    setLangauge('EN')
                    console.log('to EN')
                }}
            >EN</button>
        </div>
    )
}

const Navbar = () => {
    return (
    <div className="Navbar">
        <div className='NavLogo'>
            <img src={logo} className="Logo" />
        </div>
        <div className='NavSpace'>
            <LangaugeSelector />
        </div>
        
    </div>
    )
}
export default Navbar